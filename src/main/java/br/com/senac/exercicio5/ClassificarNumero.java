package br.com.senac.exercicio5;

import java.util.Scanner;

public class ClassificarNumero {

    public static void main(String[] args) {

        int numero;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite um numero:");
        numero = scanner.nextInt();

        if (isPar(numero)) {
            System.out.print("Este numero é Par");
        } else {
            System.out.print("Este numero é Impar");
        }

        if (isPositivo(numero)) {
            System.out.println(" Este numero é positivo");
        } else {
            System.out.println(" Este numero é negativo");
        }

    }

    public static boolean isPar(int numero) {
        return numero % 2 == 0;
    }

    public static boolean isPositivo(int numero) {
        return numero >= 0;
    }

}
