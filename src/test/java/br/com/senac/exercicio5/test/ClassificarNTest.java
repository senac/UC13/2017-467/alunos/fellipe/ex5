
package br.com.senac.exercicio5.test;


import br.com.senac.exercicio5.ClassificarNumero;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClassificarNTest {
    
    public ClassificarNTest() {
    }
    
    
    @Test
    public void numero12deveSerPar() {
        int numero = 12;
        boolean resultado = ClassificarNumero.isPar(numero);
        assertTrue(resultado);
    }

    @Test
    public void numero7NaoDeveSerPar() {
        int numero = 7;
        boolean resultado = ClassificarNumero.isPar(numero);
        assertFalse(resultado);
    }

    @Test
    public void numero10DeveSerPosivo() {
        int numero = 10;
        boolean resultado = ClassificarNumero.isPositivo(numero);
        assertTrue(resultado);
    }
    
    public void numero10NegativoDeveSerPosivo(){
         int numero = -10;
        boolean resultado = ClassificarNumero.isPositivo(numero);
        assertFalse(resultado);
    }


    
    
}
